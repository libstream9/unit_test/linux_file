#include <stream9/linux/realpath.hpp>

#include <unistd.h>

#include <boost/test/unit_test.hpp>

#include <stream9/path/absolute.hpp>

namespace testing {

using stream9::linux::realpath;
using stream9::path::is_absolute;

BOOST_AUTO_TEST_SUITE(realpath_)

    BOOST_AUTO_TEST_CASE(current_dir_)
    {
        auto p1 = realpath(".");

        BOOST_TEST(is_absolute(p1));
    }

BOOST_AUTO_TEST_SUITE_END() // realpath_

} // namespace testing
