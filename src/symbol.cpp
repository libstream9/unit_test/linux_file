#include <stream9/linux/file/symbol.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <sys/mman.h>

namespace testing {

BOOST_AUTO_TEST_CASE(fstatat_flags_to_symbol_)
{
    struct test_entry {
        int flags;
        std::string_view symbol;
    };

    test_entry entries[] = {
        { 0, "" },
        { AT_EMPTY_PATH, "AT_EMPTY_PATH", },
        { AT_NO_AUTOMOUNT, "AT_NO_AUTOMOUNT", },
        { AT_SYMLINK_NOFOLLOW, "AT_SYMLINK_NOFOLLOW", },
        { AT_EMPTY_PATH | AT_NO_AUTOMOUNT, "AT_EMPTY_PATH | AT_NO_AUTOMOUNT", },
        { AT_EMPTY_PATH | AT_SYMLINK_NOFOLLOW, "AT_EMPTY_PATH | AT_SYMLINK_NOFOLLOW", },
        { AT_NO_AUTOMOUNT | AT_SYMLINK_NOFOLLOW, "AT_NO_AUTOMOUNT | AT_SYMLINK_NOFOLLOW", },
        { AT_EMPTY_PATH | AT_NO_AUTOMOUNT | AT_SYMLINK_NOFOLLOW, "AT_EMPTY_PATH | AT_NO_AUTOMOUNT | AT_SYMLINK_NOFOLLOW", },
        { AT_EMPTY_PATH | 0x2000, "AT_EMPTY_PATH | 0x2000", },
    };

    for (auto const& ent: entries) {
        auto s = li::fstatat_flags_to_symbol(ent.flags);

        BOOST_TEST(s == ent.symbol);
    }
}

BOOST_AUTO_TEST_SUITE(mmap_prot_to_symbol_)

    BOOST_AUTO_TEST_CASE(none_)
    {
        auto s1 = li::mmap_prot_to_symbol(PROT_NONE);

        BOOST_TEST(s1 == "PROT_NONE");
    }

    BOOST_AUTO_TEST_CASE(others_)
    {
        struct test_entry {
            int prot;
            std::string_view symbol;
        };

        test_entry entries[] = {
            { PROT_NONE, "PROT_NONE" },
            { PROT_READ, "PROT_READ" },
            { PROT_WRITE, "PROT_WRITE" },
            { PROT_EXEC, "PROT_EXEC" },
            { PROT_READ | PROT_WRITE, "PROT_READ | PROT_WRITE" },
            { PROT_READ | PROT_EXEC, "PROT_READ | PROT_EXEC" },
            { PROT_WRITE | PROT_EXEC, "PROT_WRITE | PROT_EXEC" },
            { PROT_READ | PROT_WRITE | PROT_EXEC, "PROT_READ | PROT_WRITE | PROT_EXEC" },
            { PROT_READ | 0x100, "PROT_READ | 0x100" },
        };

        for (auto const& ent: entries) {
            auto s = li::mmap_prot_to_symbol(ent.prot);

            BOOST_TEST(s == ent.symbol);
        }
    }

BOOST_AUTO_TEST_SUITE_END() // mmap_prot_to_symbol_

BOOST_AUTO_TEST_SUITE(mmap_flags_to_symbol_)

    BOOST_AUTO_TEST_CASE(type_)
    {
        auto s1 = li::mmap_flags_to_symbol(MAP_PRIVATE);
        BOOST_TEST(s1 == "MAP_PRIVATE");

        auto s2 = li::mmap_flags_to_symbol(MAP_SHARED);
        BOOST_TEST(s2 == "MAP_SHARED");

        auto s3 = li::mmap_flags_to_symbol(MAP_SHARED_VALIDATE);
        BOOST_TEST(s3 == "MAP_SHARED_VALIDATE");
    }

    BOOST_AUTO_TEST_CASE(flags_)
    {
        struct entry {
            int flags;
            char const* symbols;
        };

        entry entries[] = {
            { MAP_PRIVATE | MAP_32BIT, "MAP_PRIVATE | MAP_32BIT" },
            { MAP_32BIT | MAP_ANONYMOUS, "MAP_32BIT | MAP_ANONYMOUS" },
            { MAP_ANONYMOUS | MAP_DENYWRITE, "MAP_ANONYMOUS | MAP_DENYWRITE" },
            { MAP_DENYWRITE | MAP_EXECUTABLE, "MAP_DENYWRITE | MAP_EXECUTABLE" },
            { MAP_EXECUTABLE, "MAP_EXECUTABLE" },
            { MAP_FIXED, "MAP_FIXED" },
            { MAP_FIXED | MAP_FIXED_NOREPLACE, "MAP_FIXED | MAP_FIXED_NOREPLACE" },
            { MAP_FIXED_NOREPLACE | MAP_GROWSDOWN, "MAP_FIXED_NOREPLACE | MAP_GROWSDOWN" },
            { MAP_GROWSDOWN | MAP_HUGETLB, "MAP_GROWSDOWN | MAP_HUGETLB" },
            { MAP_HUGETLB | MAP_LOCKED, "MAP_HUGETLB | MAP_LOCKED" },
            { MAP_LOCKED | MAP_NONBLOCK, "MAP_LOCKED | MAP_NONBLOCK" },
            { MAP_NONBLOCK | MAP_NORESERVE, "MAP_NONBLOCK | MAP_NORESERVE" },
            { MAP_NORESERVE | MAP_POPULATE, "MAP_NORESERVE | MAP_POPULATE" },
            { MAP_POPULATE | MAP_STACK, "MAP_POPULATE | MAP_STACK" },
            { MAP_STACK | MAP_SYNC, "MAP_STACK | MAP_SYNC" },
        };

        for (auto const& ent: entries) {
            auto s = li::mmap_flags_to_symbol(ent.flags);

            BOOST_TEST(s == ent.symbols);
        }
    }

BOOST_AUTO_TEST_SUITE_END() // mmap_flags_to_symbol_

} // namespace testing
