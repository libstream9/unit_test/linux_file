#include <stream9/linux/write_n.hpp>

#include <stream9/linux/open.hpp>

#include "namespace.hpp"

#include <array>

#include <stream9/filesystem/load_string.hpp>
#include <stream9/filesystem/temporary_file.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(write_n_)

    BOOST_AUTO_TEST_CASE(array_)
    {
        fs::temporary_file f;

        char buf[] { 'f', 'o', 'o', };

        lx::write_n(f.fd(), buf);

        auto s = fs::load_string(f.path());
        BOOST_TEST_REQUIRE(s.size() == 3);
        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(std_array_)
    {
        fs::temporary_file f;

        std::array<char, 3> buf { 'f', 'o', 'o', };

        lx::write_n(f.fd(), buf);

        auto s = fs::load_string(f.path());
        BOOST_TEST_REQUIRE(s.size() == 3);
        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_)
    {
        fs::temporary_file f;

        std::string buf = "foo";

        lx::write_n(f.fd(), buf);

        auto s = fs::load_string(f.path());
        BOOST_TEST_REQUIRE(s.size() == 3);
        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        fs::temporary_file f;

        std::string buf {};

        lx::write_n(f.fd(), buf);

        auto s = fs::load_string(f.path());
        BOOST_TEST_REQUIRE(s.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(error_1_)
    {
        std::string buf = "foo";

        BOOST_CHECK_EXCEPTION(
            lx::write_n(-1, buf),
            stream9::error,
            [&](auto&& e) {
                BOOST_TEST(e.why() == lx::ebadf);
                return true;
            });

    }

BOOST_AUTO_TEST_SUITE_END() // write_n_

BOOST_AUTO_TEST_SUITE(nothrow_write_n_)

    BOOST_AUTO_TEST_CASE(string_)
    {
        fs::temporary_file f;

        std::string buf = "foo";

        auto o_n = lx::nothrow::write_n(f.fd(), buf);

        BOOST_REQUIRE(o_n);
        BOOST_TEST(*o_n == 3);

        auto s = fs::load_string(f.path());
        BOOST_TEST_REQUIRE(s.size() == 3);
        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(error_1_)
    {
        std::string buf = "foo";

        auto o_n = lx::nothrow::write_n(-1, buf);

        BOOST_REQUIRE(!o_n);
        BOOST_TEST(o_n.error == lx::ebadf);
    }

BOOST_AUTO_TEST_SUITE_END() // nothrow_write_n_

} // namespace testing
