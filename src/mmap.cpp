#include <stream9/linux/file/mmap.hpp>

#include "namespace.hpp"

#include <stream9/linux/file/open.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/errors.hpp>
#include <stream9/filesystem.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(mmap_)

    BOOST_AUTO_TEST_CASE(from_file_)
    {
        fs::tmpfstream fs;
        fs << "hello" << std::flush;

        auto m = li::mmap(fs.path());

        BOOST_TEST(m.size() == 5);

        std::string_view s = m;
        BOOST_TEST(s == "hello");
    }

    BOOST_AUTO_TEST_CASE(from_fd_)
    {
       try {
            fs::tmpfstream fs;
            fs << "hello" << std::flush;

            auto fd = li::open(fs.path(), O_RDWR);

            auto m = li::mmap(fd);

            BOOST_TEST(m.size() == 5);

            ::memcpy(m.data(), "HELLO", 5);

       }
       catch (...) {
            stream9::errors::print_error();
       }
    }

    BOOST_AUTO_TEST_CASE(error_1_)
    {
        BOOST_CHECK_EXCEPTION(
            li::mmap("/proc/self/mountinfo"),
            stream9::errors::error,
            [&](auto&& e) {
                BOOST_TEST(e.why() == li::mmap::errc::try_to_map_empty_file);
                return true;
            });
    }

BOOST_AUTO_TEST_SUITE_END() // mmap_

} // namespace testing
