#include <stream9/linux/access.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>

namespace testing {

namespace json { using namespace stream9::json; }

BOOST_AUTO_TEST_SUITE(euidaccess_)

    BOOST_AUTO_TEST_CASE(success_)
    {
        auto r = lx::euidaccess("/", F_OK);

        BOOST_TEST(r);
    }

    BOOST_AUTO_TEST_CASE(error_)
    {
        BOOST_CHECK_EXCEPTION(
            lx::euidaccess("", F_OK),
            stream9::error,
            [&](auto& e) {
                BOOST_TEST(e.why() == lx::enoent);
                return true;
            });
    }

BOOST_AUTO_TEST_SUITE_END() // euidaccess_

} // namespace testing
