#include <stream9/linux/statvfs.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem.hpp>

namespace testing {

template<typename T>
concept magnitude = std::integral<T> || std::floating_point<T>;

template<typename T>
concept unit = requires {
    { T::numerator() } -> std::integral; //TODO non-negative
    { T::denominator() } -> std::integral; //TODO positive
    { T::prefix() } -> std::convertible_to<char const*>;
    { T::symbol() } -> std::convertible_to<char const*>;
};

struct kilo
{
    static constexpr std::intmax_t numerator() noexcept { return 1000; }
    static constexpr std::intmax_t denominator() noexcept { return 1; }
    static constexpr char const* prefix() noexcept { return "kilo"; }
    static constexpr char const* symbol() noexcept { return "k"; }
};

template<magnitude M, unit U>
class quantity
{
public:
    using magnitude_t = M;
    using unit_t = U;

public:
    quantity() = default;

    quantity(magnitude_t v) noexcept
        : m_magnitude { v }
    {}

    template<typename M2, typename U2>
    quantity(quantity<M2, U2> o) noexcept
    {
        std::intmax_t v = o.value();
    }

    // accessor
    magnitude_t value() const noexcept { return m_magnitude; }

private:
    magnitude_t m_magnitude;
};

BOOST_AUTO_TEST_SUITE(statvfs_)

    BOOST_AUTO_TEST_CASE(step_1_)
    {
#if 0
        try {
            auto st = lx::statvfs("/");

            std::cout << "capacity:  " << lx::total_bytes(st) << std::endl;
            std::cout << "used:      " << lx::used_bytes(st) << std::endl;
            std::cout << "available: " << lx::available_bytes(st) << std::endl;
            std::cout << std::setw(2) << json::value_from(st) << std::endl;
        }
        catch (...) {
            stream9::print_error();
            throw;
        }
#endif
    }

BOOST_AUTO_TEST_SUITE_END() // statvfs_

} // namespace testing
