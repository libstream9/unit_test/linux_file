#include <stream9/linux/file/mkdir.hpp>

#include "namespace.hpp"

#include <stream9/linux/file/stat.hpp>
#include <stream9/path/concat.hpp> //operator/

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem.hpp>

namespace testing {

using st9::path::operator/;

BOOST_AUTO_TEST_SUITE(mkdir_)

    BOOST_AUTO_TEST_CASE(step_1_)
    {
        try {
            fs::temporary_directory dir;

            auto p = dir.path() / "foo";

            lx::mkdir(p);

            auto o_st = lx::nothrow::stat(p);
            BOOST_REQUIRE(o_st);
            BOOST_CHECK(lx::is_directory(*o_st));
        }
        catch (...) {
            stream9::print_error();
            throw;
        }
    }

BOOST_AUTO_TEST_SUITE_END() // mkdir_

} // namespace testing
