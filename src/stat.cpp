#include <stream9/linux/file/stat.hpp>

#include "namespace.hpp"

#include <stream9/linux/file/open.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>
#include <stream9/linux/error.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(stat_)

    BOOST_AUTO_TEST_CASE(success_)
    {
        auto st = li::stat("/");

        BOOST_TEST(st.st_mode & S_IFDIR);
    }

    BOOST_AUTO_TEST_CASE(failure_)
    {
        BOOST_CHECK_EXCEPTION(
            li::stat("XXX"),
            stream9::errors::error,
            [&](auto&& e) {
                BOOST_TEST(e.why().value() == ENOENT);
                return true;
            });
    }

BOOST_AUTO_TEST_SUITE_END() // stat_

BOOST_AUTO_TEST_SUITE(fstat_)

    BOOST_AUTO_TEST_CASE(success_)
    {
        auto fd = li::open("/", O_RDONLY);
        auto st = li::fstat(fd);

        BOOST_TEST(st.st_mode & S_IFDIR);
    }

    BOOST_AUTO_TEST_CASE(failure_)
    {
        BOOST_CHECK_EXCEPTION(
            li::fstat(99999),
            stream9::errors::error,
            [&](auto&& e) {
                BOOST_TEST(e.why().value() == EBADF);
                return true;
            });
    }

BOOST_AUTO_TEST_SUITE_END() // fstat_

BOOST_AUTO_TEST_SUITE(lstat_)

    BOOST_AUTO_TEST_CASE(success_)
    {
        auto st = li::lstat("/proc/self/fd/0");

        BOOST_TEST(st.st_mode & S_IFLNK);
    }

    BOOST_AUTO_TEST_CASE(failure_)
    {
        BOOST_CHECK_EXCEPTION(
            li::lstat("/proc/self/fd/X"),
            stream9::errors::error,
            [&](auto&& e) {
                BOOST_TEST(e.why().value() == ENOENT);
                return true;
            });
    }

BOOST_AUTO_TEST_SUITE_END() // lstat_

BOOST_AUTO_TEST_SUITE(fstatat_)

    BOOST_AUTO_TEST_CASE(success_1_)
    {
        auto dirfd = li::open("/proc/self/fd", O_RDONLY);
        auto st = li::fstatat(dirfd, "0");

        BOOST_TEST(st.st_mode & S_IFLNK);
    }

    BOOST_AUTO_TEST_CASE(success_2_)
    {
        auto st = li::fstatat(AT_FDCWD, "", AT_EMPTY_PATH);

        BOOST_TEST(st.st_mode & S_IFDIR);
    }

    BOOST_AUTO_TEST_CASE(failure_)
    {
        auto dirfd = li::open("/proc/self/fd", O_RDONLY);

        BOOST_CHECK_EXCEPTION(
            li::fstatat(dirfd, "X"),
            stream9::errors::error,
            [&](auto&& e) {
                BOOST_TEST(e.why().value() == ENOENT);
                return true;
            });
    }

BOOST_AUTO_TEST_SUITE_END() // fstatat_

} // namespace testing
