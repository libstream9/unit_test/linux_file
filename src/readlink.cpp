#include <stream9/linux/file/readlink.hpp>

#include "namespace.hpp"

#include <stream9/linux/file/open.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(readlink_)

    BOOST_AUTO_TEST_CASE(success_)
    {
        auto r = li::readlink("/proc/self/fd/0");

        BOOST_TEST(r != "");
    }

    BOOST_AUTO_TEST_CASE(failure_)
    {
        BOOST_CHECK_EXCEPTION(
            li::readlink("/"),
            stream9::errors::error,
            [&](auto&& e) {
                BOOST_TEST(e.why().value() == EINVAL);
                return true;
            });
    }

BOOST_AUTO_TEST_SUITE_END() // readlink_

BOOST_AUTO_TEST_SUITE(readlinkat_)

    BOOST_AUTO_TEST_CASE(success_)
    {
        auto dirfd = open("/proc/self/fd", O_RDONLY);
        auto r = li::readlinkat(dirfd, "0");

        BOOST_TEST(r != "");
    }

    BOOST_AUTO_TEST_CASE(failure_)
    {
        BOOST_CHECK_EXCEPTION(
            li::readlinkat(AT_FDCWD, "XXX"),
            stream9::errors::error,
            [&](auto&& e) {
                BOOST_TEST(e.why().value() == ENOENT);
                return true;
            });
    }

BOOST_AUTO_TEST_SUITE_END() // readlinkat_

} // namespace testing
