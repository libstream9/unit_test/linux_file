#include <stream9/linux/link.hpp>

#include "namespace.hpp"

#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/linux/open.hpp>
#include <stream9/linux/stat.hpp>
#include <stream9/linux/write_n.hpp>
#include <stream9/path/concat.hpp> // operator/

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(linkat_)

    BOOST_AUTO_TEST_CASE(from_fd_)
    {
        using st9::path::operator/;

        try {
            fs::temporary_directory dir;

            auto fd = lx::open(dir.path(), O_RDWR | O_TMPFILE, 0666);
            auto st1 = lx::fstat(fd);

            lx::write_n(fd, "Hello, world");

            auto newpath = dir.path() / "foo";
            lx::linkat(fd, newpath);
            auto st2 = lx::stat(newpath);

            BOOST_TEST(st1.st_ino == st2.st_ino);
        }
        catch (...) {
            stream9::print_error();
        }
    }

BOOST_AUTO_TEST_SUITE_END() // linkat_

} // namespace testing
