#include <stream9/linux/mkstemp.hpp>

#include "namespace.hpp"

#include <stream9/linux/unlink.hpp>
#include <stream9/strings/size.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(mkstemp_)

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        auto [fd, p] = lx::mkstemp();

        BOOST_TEST(p.size() == 6);

        lx::unlink(p);
    }

    BOOST_AUTO_TEST_CASE(with_prefix_)
    {
        using str::size;

        auto tmpl = "tmpf_";
        auto [fd, p] = lx::mkstemp("tmpf_");

        BOOST_TEST(p.size() == size(tmpl) + 6);

        lx::unlink(p);
    }

BOOST_AUTO_TEST_SUITE_END() // mkstemp_

} // namespace testing
