#include <stream9/linux/mkdtemp.hpp>

#include "namespace.hpp"

#include <stream9/linux/rmdir.hpp>
#include <stream9/strings/size.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(mkdtemp_)

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        auto p = lx::mkdtemp();

        BOOST_TEST(p.size() == 6);

        lx::rmdir(p);
    }

    BOOST_AUTO_TEST_CASE(with_prefix_)
    {
        using stream9::strings::size;

        auto tmpl = "tmpdir_";
        auto p = lx::mkdtemp("tmpdir_");

        BOOST_TEST(p.size() == size(tmpl) + 6);

        lx::rmdir(p);
    }

BOOST_AUTO_TEST_SUITE_END() // mkdtemp_

} // namespace testing
