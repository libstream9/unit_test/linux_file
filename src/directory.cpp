#include <stream9/linux/file/directory.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem/temporary_directory.hpp>

#include <unistd.h>

#include <fstream>

namespace testing {

BOOST_AUTO_TEST_SUITE(directory_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        static_assert(std::ranges::input_range<lx::directory>);
        static_assert(!std::copyable<lx::directory>);
        static_assert(std::movable<lx::directory>);

        static_assert(std::input_iterator<lx::directory::const_iterator>);
    }

    BOOST_AUTO_TEST_CASE(constructor_1_)
    {
        lx::directory dir { "/" };

        BOOST_TEST(dir.path() == "/");
    }

    BOOST_AUTO_TEST_CASE(tell_)
    {
        lx::directory dir { "/" };

        auto it = dir.begin();

        BOOST_TEST(dir.tell() == (*it).d_off);
    }

    BOOST_AUTO_TEST_CASE(type_)
    {
        fs::temporary_directory tmp;

        auto fpath = tmp / "foo";
        std::ofstream fs { fpath.c_str() };
        fs << "foo";
        fs.close();

        auto lname = "bar";
        auto lpath = tmp / lname;
        auto rc = ::symlink(fpath.c_str(), lpath.c_str());
        BOOST_REQUIRE(rc != -1);

        bool ok1 = false, ok2 = false, ok3 = false;

        lx::directory dir { tmp.path() };
        for (auto& e: dir) {
            if (lx::name(e) == "foo") {
                BOOST_TEST(lx::ltype(e, dir.fd()) == DT_REG);
                ok1 = true;
            }
            else if (lx::name(e) == "bar") {
                BOOST_TEST(lx::ltype(e, dir.fd()) == DT_LNK);
                ok2 = true;
                BOOST_TEST(lx::type(e, dir.fd()) == DT_REG);
                ok3 = true;
            }
        }

        BOOST_TEST(ok1);
        BOOST_TEST(ok2);
        BOOST_TEST(ok3);
    }

BOOST_AUTO_TEST_SUITE_END() // directory_

} // namespace testing
