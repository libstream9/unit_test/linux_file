#include <stream9/linux/file/rename.hpp>

#include "namespace.hpp"

#include <stream9/linux/file/open.hpp>
#include <stream9/linux/file/stat.hpp>
#include <stream9/path/concat.hpp> // operator/

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem.hpp>

namespace testing {

namespace path = stream9::path;

using path::operator/;

BOOST_AUTO_TEST_SUITE(rename_)

    BOOST_AUTO_TEST_CASE(step_1_)
    {
        try {
            fs::temporary_directory dir;

            auto f = lx::open(dir.path() / "foo", O_CREAT | O_WRONLY);
            f.close();

            lx::rename(dir.path() / "foo", dir.path() / "bar");

            auto o_st = lx::nothrow::stat(dir.path() / "bar");
            BOOST_CHECK(o_st);
        }
        catch (...) {
            stream9::print_error();
            throw;
        }
    }

BOOST_AUTO_TEST_SUITE_END() // rename_

} // namespace testing
