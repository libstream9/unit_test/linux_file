#include <stream9/linux/file/statfs.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(statfs_)

#if 0
    BOOST_AUTO_TEST_CASE(step_1_)
    {
        try {
            auto st = lx::statfs("/");

            std::cout << std::setw(2) << json::value_from(st) << std::endl;
        }
        catch (...) {
            stream9::print_error();
            throw;
        }
    }
#endif

BOOST_AUTO_TEST_SUITE_END() // statfs_

} // namespace testing

