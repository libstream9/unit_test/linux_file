#include <stream9/linux/getcwd.hpp>

#include <unistd.h>

#include <boost/test/unit_test.hpp>

#include <stream9/path/absolute.hpp>

namespace testing {

using stream9::linux::getcwd;
using stream9::path::is_absolute;

BOOST_AUTO_TEST_SUITE(getcwd_)

    BOOST_AUTO_TEST_CASE(basic_1_)
    {
        auto p1 = getcwd();

        BOOST_TEST(is_absolute(p1));
    }

BOOST_AUTO_TEST_SUITE_END() // is_absolute_

} // namespace testing
