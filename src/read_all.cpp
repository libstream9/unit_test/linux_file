#include <stream9/linux/file/utility/read_all.hpp>

#include "namespace.hpp"

#include <stream9/linux/open.hpp>
#include <stream9/linux/write.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(read_all_)

    BOOST_AUTO_TEST_CASE(string_)
    {
        fs::tmpfstream os;
        os << "foo" << std::flush;

        auto fd = lx::open(os.path(), O_RDONLY);
        std::string s;
        lx::read_all(fd, s);

        BOOST_TEST(s == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // read_all_

} // namespace testing
