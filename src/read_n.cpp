#include <stream9/linux/read_n.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/linux/open.hpp>
#include <stream9/string.hpp>

namespace testing {

using st9::string;

BOOST_AUTO_TEST_SUITE(read_n_)

    BOOST_AUTO_TEST_CASE(basic_1_)
    {
        string s;
        s.resize(100);

        auto fd = lx::open("/etc/passwd", O_RDONLY);

        auto oc = lx::read_n(fd, s);

        BOOST_TEST(!!oc);
        BOOST_TEST(*oc == 100);
        BOOST_TEST(!oc.is_eof);
    }

BOOST_AUTO_TEST_SUITE_END() // read_n_

} // namespace testing
